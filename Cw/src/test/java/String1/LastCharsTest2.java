package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class LastCharsTest2 {
    @Test
    public void lastCharsTest2 () {
        //Given
        String str1 = "yo";
        String str2 = "java";

        //When
        String result = new LastChars().lastChars(str1,str2);

        //Then
        assertEquals(result,"ya");
    }
}
