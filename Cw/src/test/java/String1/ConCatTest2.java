package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class ConCatTest2 {
    @Test
    public void conCatTest2 () {
        //Given
        String str1 = "dog";
        String str2 = "cat";

        //When
        String result = new ConCat().conCat(str1,str2);

        //Then
        assertEquals(result,"dogcat");
    }
}
