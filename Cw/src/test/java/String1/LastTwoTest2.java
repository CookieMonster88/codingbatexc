package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class LastTwoTest2 {
    @Test
    public void lastTwoTest2 () {
        //Given
        String str = "cat";

        //When
        String result = new LastTwo().lastTwo(str);

        //Then
        assertEquals(result,"cta");
    }
}
