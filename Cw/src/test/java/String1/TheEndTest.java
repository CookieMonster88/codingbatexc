package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class TheEndTest {
    @Test
    public void theEndTest () {
        //Given
        String str = "Hello";
        boolean front = true;

        //When
        String result = new TheEnd().theEnd(str,front);

        //Then
        assertEquals(result,"H");
    }
}
