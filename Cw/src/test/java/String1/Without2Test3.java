package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-25.
 */
public class Without2Test3 {
    @Test
    public void withoutTest3 () {
        //Given
        String str = "Hi";

        //When
        String result = new Without2().without2(str);

        //Then
        assertEquals(result,"");
    }
}
