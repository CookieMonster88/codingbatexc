package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class MiddleThreeTest1 {
    @Test
    public void middleThreeTest () {
        //Given
        String str = "solving";

        //When
        String result = new MiddleThree().middleThree(str);

        //Then
        assertEquals(result,"lvi");
    }
}
