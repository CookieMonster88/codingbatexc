package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Created by RENT on 2017-01-23.
 */
public class AtFirstTest3 {
    @Test
    public void atFirstTest3 () {
        //Given
        String str = "H";

        //When
        String result = new AtFirst().atFirst(str);

        //Then
        assertEquals(result,"H@");
    }
}
