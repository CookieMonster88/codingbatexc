package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class LastCharsTest {
    @Test
    public void lastCharsTest () {
        //Given
        String str1 = "last";
        String str2 = "chars";

        //When
        String result = new LastChars().lastChars(str1,str2);

        //Then
        assertEquals(result,"ls");
    }
}
