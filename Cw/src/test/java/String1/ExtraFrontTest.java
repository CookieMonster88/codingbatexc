package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-25.
 */
public class ExtraFrontTest {
    @Test
    public void extraFrontTest () {
        //Given
        String str = "Hello";

        //When
        String result = new Extrafront().extraFront(str);

        //Then
        assertEquals(result,"HeHeHe");
    }
}
