package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */
public class MakeAbbaTest {
    @Test
    public void makeAbbaTest () {
        //Given
        String str1 = "Hey";
        String str2 = "Bye";

        //When
        String result = new MakeAbba().makeAbba(str1,str2);

        //Then
        assertEquals(result,"HeyByeByeHey");
    }
}
