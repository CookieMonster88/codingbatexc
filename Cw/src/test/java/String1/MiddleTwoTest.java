package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class MiddleTwoTest {
    @Test
    public void middleTwoTest () {
        //Given
        String str = "string";

        //When
        String result = new MiddleTwo().middleTwo(str);

        //Then
        assertEquals(result,"ri");
    }
}
