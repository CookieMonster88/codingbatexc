package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class LastTwoTest3 {
    @Test
    public void lastTwoTest3 () {
        //Given
        String str = "ab";

        //When
        String result = new LastTwo().lastTwo(str);

        //Then
        assertEquals(result,"ba");
    }
}
