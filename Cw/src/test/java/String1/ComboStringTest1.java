package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class ComboStringTest1 {
    @Test
    public void comboStringTest1 () {

        //Given
        String a = "Hi";
        String b = "Hello";

        //When
        String result = new ComboString().comboString(a,b);

        //Then
        assertEquals(result,"HiHelloHi");

    }
}
