package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class MinCatTest3 {
    @Test
    public void minCatTest3 () {
        //Given
        String str1 = "java";
        String str2 = "Hello";

        //When
        String result = new MinCat().minCat(str1,str2);

        //Then
        assertEquals(result,"javaello");
    }
}
