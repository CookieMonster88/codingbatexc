package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class MinCatTest2 {
    @Test
    public void minCatTest2 () {
        //Given
        String str1 = "Hello";
        String str2 = "java";

        //When
        String result = new MinCat().minCat(str1,str2);

        //Then
        assertEquals(result,"ellojava");
    }
}
