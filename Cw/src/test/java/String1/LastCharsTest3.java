package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class LastCharsTest3 {
    @Test
    public void lastCharsTest3 () {
        //Given
        String str1 = "hi";
        String str2 = "";

        //When
        String result = new LastChars().lastChars(str1,str2);

        //Then
        assertEquals(result,"h@");
    }
}
