package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-25.
 */
public class DeFrontTest {
    @Test
    public void deFrontTest () {
        //Given
        String str = "Hello";

        //When
        String result = new DeFront().deFront(str);

        //Then
        assertEquals(result,"llo");
    }
}
