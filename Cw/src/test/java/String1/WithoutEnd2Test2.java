package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class WithoutEnd2Test2 {
    @Test
    public void withoutEnd2Test2 () {
        //Given
        String str = "a";

        //When
        String result = new WithouEnd2().withouEnd2(str);

        //Then
        assertEquals(result,"");
    }
}
