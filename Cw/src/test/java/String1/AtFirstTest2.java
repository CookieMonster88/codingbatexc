package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class AtFirstTest2 {
    @Test
    public void atFirstTest2 () {
        //Given
        String str = "Hi";

        //When
        String result = new AtFirst().atFirst(str);

        //Then
        assertEquals(result,"Hi");
    }
}
