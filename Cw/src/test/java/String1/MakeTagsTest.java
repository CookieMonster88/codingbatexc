package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */
public class MakeTagsTest {
    @Test
    public void makeTagsTest () {
        //Given
        String str1 = "<i>";
        String str2 = "word";

        //When
        String result = new MakeTags().makeTags(str1,str2);

        //Then
        assertEquals(result,"<i>word<i>");
    }
}
