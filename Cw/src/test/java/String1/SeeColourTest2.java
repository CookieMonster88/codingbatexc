package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class SeeColourTest2 {
    @Test
    public void seeColourTest2 () {
        //Given
        String str = "xxxred";

        //When
        String result = new SeeColour().seeColour(str);

        //Then
        assertEquals(result,"");
    }
}
