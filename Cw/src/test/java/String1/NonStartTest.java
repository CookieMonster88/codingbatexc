package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class NonStartTest {
    @Test
    public void nonStartTest () {
        //Given
        String a = "Hello";
        String b = "There";

        //When
        String result = new NonStart().nonStart(a,b);

        //Then
        assertEquals(result,"ellohere");
    }
}
