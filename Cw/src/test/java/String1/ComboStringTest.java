package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class ComboStringTest {
    @Test
    public void comboStringTest () {
        //Given
        String a = "Hello";
        String b = "Hi";

        //When
        String result = new ComboString().comboString(a,b);

        //Then
        assertEquals(result,"HiHelloHi");
    }
}
