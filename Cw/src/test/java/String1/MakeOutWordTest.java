package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */
public class MakeOutWordTest {
    @Test
    public void makeOutTest () {
        //Given
        String str1 = "<<>>";
        String str2 = "word";

        //When
        String result = new MakeOutWord().makeOutString(str1,str2);

        //Then
        assertEquals(result,"<<word>>");
    }
}
