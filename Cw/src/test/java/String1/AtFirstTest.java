package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class AtFirstTest {
    @Test
    public void atFirstTest () {
        //Given
        String str = "Hello";

        //When
        String result = new AtFirst().atFirst(str);

        //Then
        assertEquals(result,"He");
    }
}
