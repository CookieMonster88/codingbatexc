package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */

public class WithoutEndTest {
    @Test
    public void withoutEnd () {
        // Given
        String str = "Hello";

        //When
        String result = new WithoutEnd().withoutEnd(str);

        //Then
        assertEquals(result,"ell");
    }
}
