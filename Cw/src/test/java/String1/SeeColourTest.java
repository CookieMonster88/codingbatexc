package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class SeeColourTest {
    @Test
    public void seeColourTest () {
        //Given
        String str = "redxxx";

        //When
        String result = new SeeColour().seeColour(str);

        //Then
        assertEquals(result,"red");
    }
}
