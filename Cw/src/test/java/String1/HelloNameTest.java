package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */

public class HelloNameTest {
    @Test
    public void helloNameTest () {
        //Given
        String str = "Bob";

        //When
        String result = new HelloName().helloName(str);

        //Then
        assertEquals(result,"HelloBob");
    }
}
