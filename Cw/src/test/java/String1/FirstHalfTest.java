package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */
public class FirstHalfTest {
    @Test
    public void firstHalfTest () {
        //Given
        String str = "WooHoo";

        //When
        String result = new FirstHalf().firstHalf(str);

        //Then
        assertEquals(result,"Woo");
    }
}
