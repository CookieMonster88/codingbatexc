package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class ConCatTest {
    @Test
    public void conCatTest () {
        //Given
        String str1 = "abc";
        String str2 = "cat";

        //When
        String result = new ConCat().conCat(str1,str2);

        //Then
        assertEquals(result,"abcat");
    }
}
