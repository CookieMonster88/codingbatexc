package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class Left2Test {
    @Test
    public void left2Test () {
        //Given
        String str = "Hello";

        //When
        String result = new Left2().left2(str);

        //Then
        assertEquals(result,"lloHe");
    }
}
