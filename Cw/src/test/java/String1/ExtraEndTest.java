package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */
public class ExtraEndTest {
    @Test
    public void extraEndTest () {
        //Given
        String str = "Hello";

        //When
        String result = new ExtraEnd().extraEnd(str);

        //Then
        assertEquals(result,"lololo");
    }
}
