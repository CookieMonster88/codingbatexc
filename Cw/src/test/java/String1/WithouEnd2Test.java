package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class WithouEnd2Test {
    @Test
    public void withouEnd2Test () {
    //Given
        String str = "Hello";

    //When
        String result = new WithouEnd2().withouEnd2(str);

    //Then
        assertEquals(result,"ell");
    }
}
