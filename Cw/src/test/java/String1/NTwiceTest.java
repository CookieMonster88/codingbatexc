package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class NTwiceTest {
    @Test
    public void nTwiceTest () {
        //Given
        String str = "Hello";
        int n = 2;

        //When
        String result = new NTwice().nTwice(str,n);

        //Then
        assertEquals(result,"Helo");
    }
}
