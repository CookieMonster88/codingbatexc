package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class TheEndTest2 {
    @Test
    public void theEndTest2 () {
        //Given
        String str = "Hello";
        boolean front = false;

        //When
        String result = new TheEnd().theEnd(str,front);

        //Then
        assertEquals(result,"o");
    }
}
