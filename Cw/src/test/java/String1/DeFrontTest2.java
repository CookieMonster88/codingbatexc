package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-25.
 */
public class DeFrontTest2 {
    @Test
    public void deFrontTest2 () {
        //Given
        String str = "away";

        //When
        String result = new DeFront().deFront(str);

        //Then
        assertEquals(result,"aay");
    }
}
