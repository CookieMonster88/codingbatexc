package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class LastTwoTest {
    @Test
    public void lastTwoTest () {
        //Given
        String str = "coding";

        //When
        String result = new LastTwo().lastTwo(str);

        //Then
        assertEquals(result,"codign");
    }
}
