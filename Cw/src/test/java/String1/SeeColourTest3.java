package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-24.
 */
public class SeeColourTest3 {
    @Test
    public void seeColourTest3 () {
        //Given
        String str = "blueTimes";

        //When
        String result = new SeeColour().seeColour(str);

        //Then
        assertEquals(result,"blue");
    }
}
