package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-23.
 */
public class MiddleThreeTest {
    @Test
    public void middleThreeTest () {
        //Given
        String str = "Candy";

        //When
        String result = new MiddleThree().middleThree(str);

        //Then
        assertEquals(result,"and");
    }
}
