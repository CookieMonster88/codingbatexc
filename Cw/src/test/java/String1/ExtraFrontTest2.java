package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-25.
 */
public class ExtraFrontTest2 {
    @Test
    public void extraFrontTest2 () {
        //Given
        String str = "ab";

        //When
        String result = new Extrafront().extraFront(str);

        //Then
        assertEquals(result,"ababab");
    }
}
