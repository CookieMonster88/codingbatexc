package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-19.
 */
public class FirstTwoTest {
    @Test
    public void firstTwoTest () {
        //Given
        String str = "Hello";

        //When
        String result = new FirstTwo().firstTwo(str);

        //Then
        assertEquals(result,"He");
    }
}
