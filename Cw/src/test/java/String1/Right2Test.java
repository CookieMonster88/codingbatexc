package String1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-21.
 */
public class Right2Test {
    @Test
    public void right2Test () {
        //Given
        String str = "Hello";

        //When
        String result = new Right2().right2(str);

        //Then
        assertEquals(result,"loHel");

    }
}
