package String2;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Beata on 2017-02-08.
 */
public class EndOtherTest3 {
    @Test
    public void endOtherTest3 () {
        //Given
        String str1 = "abc";
        String str2 = "abXabc";

        //When
        boolean result = new EndOther().endOther(str1,str2);

        //Then
        assertThat("Correct",true);
    }
}
