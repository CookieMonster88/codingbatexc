package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class DoubleCharTest2 {
    @Test
    public void doubleCharTest2 () {
        //Given
        String str = "AAbb";

        //When
        String result = new DoubleChar().doubleChar(str);

        //Then
        assertEquals(result,"AAAAbbbb");
    }
}
