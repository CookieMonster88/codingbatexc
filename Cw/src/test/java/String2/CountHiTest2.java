package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class CountHiTest2 {
    @Test
    public void countHiTest2 () {
        //Given
        String str = "ABChi hi";

        //When
        int result = new CountHi().countHi(str);

        //Then
        assertEquals(result,2);
    }
}
