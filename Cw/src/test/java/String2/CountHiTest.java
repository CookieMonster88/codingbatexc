package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class CountHiTest {
    @Test
    public void countHiTest () {
        //Given
        String str = "abc hi no";

        //When
        int result = new CountHi().countHi(str);

        //Then
        assertEquals(result,1);
    }
}
