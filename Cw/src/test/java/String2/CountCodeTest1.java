package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class CountCodeTest1 {
    @Test
    public void countCodeTest1 () {
        //Given
        String str = "aaacodebbb";

        //When
        int result = new CountCode().countCode(str);

        //Then
        assertEquals(result,1);
    }
}
