package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class CountCodeTest3 {
    @Test
    public void countCodeTest3 () {
        //Given
        String str = "cozexxcope";

        //When
        int result = new CountCode().countCode(str);

        //Then
        assertEquals(result,2);
    }
}
