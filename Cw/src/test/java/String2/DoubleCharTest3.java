package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class DoubleCharTest3 {
    @Test
    public void doubleCharTest3 () {
        //Given
        String str = "Hi-There";

        //When
        String result = new DoubleChar().doubleChar(str);

        //Then
        assertEquals(result,"HHii--TThheerree");
    }
}
