package String2;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Beata on 2017-02-08.
 */
public class EndOtherTest2 {
    @Test
    public void endOtherTest2 () {
        //Given
        String a = "AbC";
        String b ="HiaBc";

        //When
        boolean result = new EndOther().endOther(a,b);

        //Then
        assertThat("Correct", true);
    }
}
