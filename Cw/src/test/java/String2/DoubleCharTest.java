package String2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-08.
 */
public class DoubleCharTest {
    @Test
    public void doubleCharTest () {
        //Given
        String str = "The";

        //When
        String result = new DoubleChar().doubleChar(str);

        //Then
        assertEquals(result,"TThhee");
    }
}
