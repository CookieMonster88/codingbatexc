package Warmup2;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-12.
 */
public class ArrayCountTest {
    @Test
    public void arrayCount9Test () {
        //Given
        int [] numbs = {9,1,4};

        //When
        int result = new ArrayCount9().arrayCount9(numbs);

        //Then
        assertEquals(result,1);

    }
}
