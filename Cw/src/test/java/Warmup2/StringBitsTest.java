package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-12.
 */
public class StringBitsTest {
    @Test
    public void stringBitsTest () {
        //Given
        String str = "Hello";

        //When
        String result = new StringBits().stringBits(str);

        //Then
        assertEquals(result,"Hlo");
    }
}
