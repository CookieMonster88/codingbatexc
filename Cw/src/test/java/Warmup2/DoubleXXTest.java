package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-12.
 */
public class DoubleXXTest {
    @Test
    public void doubleXTest () {
        //Given
        String str = "axxb";

        //When
        boolean result = new DoubleXX().doubleXX(str);

        //Then
        assertTrue("there is double xx",true);
    }
}
