package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-12.
 */
public class StringSplosionTest {
    @Test
    public void stringSplosionTest () {
        //Given
        String str = "Code";

        //When
        String result = new StringSplosion().stringSplosion(str);

        //Then
        assertEquals(result,"CCoCodCode");
    }
}
