package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-10.
 */
public class FrontTimesTest {
    @Test
    public void frontTimesTest () {
        //Given
        String str = "Chocolate";
        int n = 2;

        //When
        String result = new FrontTimes().frontTimes(str,n);

        //Then
        assertEquals(result,"ChoCho");
    }
}
