package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-12.
 */
public class ArrayCount9Test1 {
    @Test
    public void arrayCount9Test1 () {
        //Given
        int [] numbs = {9,9,3};

        //When
        int result = new ArrayCount9().arrayCount9(numbs);

        //Then
        assertEquals(result,2);

    }
}
