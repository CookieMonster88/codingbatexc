package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-12.
 */
public class ArrayFront9Test1 {
    @Test
    public void arrayFront9Test1 () {
        //Given
        int [] numbs = {1,2,3,4,9};

        //When
        boolean result = new ArrayFront9().arrayFront9(numbs);

        //Then
        assertFalse("There`s no 9 in first four numbers",false);
    }
}
