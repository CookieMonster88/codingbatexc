package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-12.
 */
public class ArrayFront9Test {
    @Test
    public void arrayFront9Test () {
        //Given
        int [] numbs = {9,1,2,3,4};

        //When
        boolean result = new ArrayFront9().arrayFront9(numbs);

        //Then
        assertTrue("There`s 9 in first four numbers",true);
    }
}
