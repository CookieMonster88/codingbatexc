package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-12.
 */
public class Array123Test1 {
    @Test
    public void array123Test1 () {
        //Given
        int [] numbers =  { 1,2,5,1,2,4,1,2,6};

        //When
        boolean result = new Array123().array123(numbers);

        //Then
        assertFalse("There`s no 1,2,3 numbers",false);

    }
}
