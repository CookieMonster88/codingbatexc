package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-12.
 */
public class DoubleXXTest1 {
    @Test
    public void doubleTest1 () {
        //Given
        String str = "axaxax";

        //When
        boolean result = new DoubleXX().doubleXX(str);

        //Then
        assertFalse("There is no double xx in String",false);
    }
}
