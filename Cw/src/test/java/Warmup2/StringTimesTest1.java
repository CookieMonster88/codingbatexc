package Warmup2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-10.
 */
public class StringTimesTest1 {
    @Test
    public void stringTimesTest1 () {
        //Given
        String str = "Cat";
        int n = 3;


        //When
        String result = new StringTimes().stringTimes(str,n);

        //Then
        assertEquals(result,"CatCatCat");
    }
}
