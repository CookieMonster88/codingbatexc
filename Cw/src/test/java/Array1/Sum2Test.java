package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-31.
 */
public class Sum2Test {
    @Test
    public void sum2Test () {
        //Given
        int [] nums = {1,2,3};

        //When
        int result = new Sum2().sum2(nums);

        //Then
        assertEquals (result,3);
    }
}
