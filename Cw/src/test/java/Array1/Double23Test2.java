package Array1;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by RENT on 2017-01-31.
 */
public class Double23Test2 {
    @Test
    public void double23Test2 () {
        //Given
        int [] nums = { 3,3 };

        //When
        boolean result = new Double23().double23(nums);

        //Then
        assertThat("Correct - double 3",true);
    }
}
