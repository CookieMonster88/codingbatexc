package Array1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by RENT on 2017-01-31.
 */
public class No23Test3 {
    @Test
    public void no23Test3 () {
        //Given
        int [] nums = { 3,5 };

        //When
        boolean result = new No23().no23(nums);

        //Then
        assertFalse("Wrong there`s a 3",false);
    }
}
