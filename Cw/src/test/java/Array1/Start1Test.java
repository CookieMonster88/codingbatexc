package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-06.
 */
public class Start1Test {
    @Test
    public void start1Test () {
        //Given
        int [] a = { 1,2,3 };
        int [] b = { 1,3 };

        //When
        int result = new Start1().start1(a,b);

        //Then
        assertEquals(result,2);
    }
}
