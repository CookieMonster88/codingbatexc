package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-06.
 */
public class Start1Test2 {
    @Test
    public void start1Test2 () {
        //Given
        int [] a = { 7,2,3 };
        int [] b = { 1 };

        //When
        int result = new Start1().start1(a,b);

        //Then
        assertEquals(result,1);
    }
}
