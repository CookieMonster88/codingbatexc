package Array1;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by RENT on 2017-01-31.
 */
public class Has23Test {
    @Test
    public void has23Test () {
        //Given
        int [] nums = { 2,5 };

        //When
        boolean result = new Has23().has23(nums);

        //Then
        assertThat("Has 2",true);
    }
}
