package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-06.
 */
public class Start1Test3 {
    @Test
    public void start1Test3 () {
        //Given
        int [] a = { 1,2 };
        int [] b = {};

        //When
        int result = new Start1().start1(a,b);

        //Then
        assertEquals(result,1);
    }
}
