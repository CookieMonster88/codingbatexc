package Array1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by RENT on 2017-01-31.
 */
public class Has23Test3 {
    @Test
    public void has23Test3 () {
        //Given
        int [] nums = { 4, 5 };

        //When
        boolean result = new Has23().has23(nums);

        //Then
        assertFalse("No numbers 2 or 3",false);
    }
}
