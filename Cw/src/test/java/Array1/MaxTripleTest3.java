package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-06.
 */
public class MaxTripleTest3 {
    @Test
    public void maxTripleTest3 () {
        //Given
        int [] nums = { 7,2,3 };

        //When
        int result = new MaxTripple().maxTripple(nums);

        //Then
        assertEquals(result,7);
    }
}
