package Array1;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by RENT on 2017-01-31.
 */
public class Has23Test2 {
    @Test
    public void has23Test2 () {
        //Given
        int [] nums = { 4,3 };

        //When
        boolean result = new Has23().has23(nums);

        //Then
        assertThat("Has 3",true);
    }
}
