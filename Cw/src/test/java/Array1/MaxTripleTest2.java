package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-02-06.
 */
public class MaxTripleTest2 {
    @Test
    public void maxTripleTest2 () {
        //Given
        int [] nums = { 1,5,3 };

        //When
        int result = new MaxTripple().maxTripple(nums);

        //Then
        assertEquals(result,5);
    }
}
