package Array1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by RENT on 2017-01-26.
 */
public class Sum3Test {
    @Test
    public void sum3Test () {
        //Given
        int [] nums = {1,2,3};

        //When
        int result = new Sum3().sum3(nums);

        //Then
        assertEquals(result,6);
    }
}
