package Array1;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by RENT on 2017-01-31.
 */
public class Double23Test {
    @Test
    public void double23Test () {
        //Given
        int [] nums = { 2,2 };

        //When
        boolean result = new Double23().double23(nums);

        //Then
        assertThat("true - double 22",true);
    }
}
