package Array1;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by RENT on 2017-01-26.
 */
public class CommonEndTest2 {
    @Test
    public void commonEndTest2 () {
        //Given
        int [] a = {1,2,3};
        int [] b = {1,4,5};

        //When
        boolean result = new CommonEnd().commonEnd(a,b);

        //Then
        assertThat("correct",true);
    }
}
