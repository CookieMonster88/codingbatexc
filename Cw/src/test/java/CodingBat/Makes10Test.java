package CodingBat;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-02.
 */
public class Makes10Test {

    @Test
    public void makes10Test () {

        //Given
        int a = 5;
        int b = 5;
        int sum = a + b;

        //When
        assertTrue("There are number 10", a == 10 || b == 10 || sum == 10);

        //Then
        System.out.println("One of numbers are 10");

    }
}
