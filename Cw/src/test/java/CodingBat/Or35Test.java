package CodingBat;

import CodingBat.Or35;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-03.
 */
public class Or35Test {
    @Test
    public void or35Test () {
        //Given
        int n = 15;


        //When
        boolean result = new Or35().or35(n);

        //Then
        assertTrue("Number can has rest from 15",true);
    }
}
