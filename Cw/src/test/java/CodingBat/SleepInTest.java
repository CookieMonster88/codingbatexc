package CodingBat;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Pawel on 2017-01-02.
 */
public class SleepInTest {

    @Test
    public void sleepInTest (){

        //given
        boolean weekday = false;
        boolean vacation = true;

        //when
        assertFalse("It`s Monday :(...",weekday);
        assertTrue("But I have now vacation!", vacation);

        //then
        System.out.println("Maybe it`s monday, but I have vacation!");
    }


}
