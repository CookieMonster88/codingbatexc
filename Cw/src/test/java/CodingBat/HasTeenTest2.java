package CodingBat;

import CodingBat.HasTeen;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-10.
 */
public class HasTeenTest2 {
    @Test
    public void hasTeenTest2 () {
        //Given
        int a2 = 6;
        int b2 = 15;
        int c2 = 33;

        //When
        boolean result = new HasTeen().hasTeen(a2,b2,c2);

        //Then
        assertTrue("There is a Teen in b",true);
    }
}
