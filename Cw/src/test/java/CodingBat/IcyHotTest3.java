package CodingBat;

import CodingBat.IcyHot;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class IcyHotTest3 {
    @Test
    public void icyHotTest3 () {
        //Given
        int temp1 = -23;
        int temp2 = -15;

        //When
        boolean result = new IcyHot().icyHot(temp1,temp2);

        //Then
        assertFalse("both temp are below 0",false);
    }
}
