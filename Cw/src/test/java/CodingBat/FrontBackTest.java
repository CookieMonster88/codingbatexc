package CodingBat;

import CodingBat.FrontBack;
import org.junit.Test;


import static org.junit.Assert.assertEquals;


/**
 * Created by Beata on 2017-01-03.
 */
public class FrontBackTest {
    @Test
    public void frontBackTest () {
        //Given
        String str = "abcd";

        //When
        String result = new FrontBack().fronBack(str);

        //Then
        assertEquals(result,"dbca");
    }
}
