package CodingBat;

import org.junit.Test;

import static org.junit.Assert.assertTrue;


/**
 * Created by Beata on 2017-01-02.
 */
public class SumDoubleTest {

    @Test
    public void sumDoubleTest() {
        //Given
        int a = 3;
        int b = 3;
        int sum = a + b;

        //When
        assertTrue("Numbers are the same", a==b );

        //Then
        sum = sum *2;
        System.out.println("Sum is now " + sum);

    }
}
