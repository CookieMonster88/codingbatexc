package CodingBat;

import CodingBat.ParrotTrouble;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class ParrotTroubleTest1 {
    @Test
    public void parrotTroubleTest () {
        //Given
        boolean parrotTalking = false;
        int hour1 = 14;

        //When
        boolean result = new ParrotTrouble().parrotTrouble(parrotTalking,hour1);

        //Then
        assertFalse("The hour is 14.00",false);
    }
}
