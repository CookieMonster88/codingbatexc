package CodingBat;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-02.
 */
public class PostNegTest {
    @Test
    public void postNegTest () {
        //Given
        int a = 3;
        int b = -6;
        boolean positive = true;
        boolean negative = false;

        //When
        assertTrue("One number is pos other is negative or both are neg", a > 0 && b < 0 || a < 0 && b < 0  || a < 0 &&
                b > 0);

        //Then
        System.out.println("One number is pos other is negative or both are neg");
    }
}
