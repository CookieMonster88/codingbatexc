package CodingBat;

import CodingBat.IcyHot;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-10.
 */
public class IcyHotTest2 {
    @Test
    public void icyHotTest2 () {
        //Given
        int t1 = 123;
        int t2 = -12;

        //When
        boolean result = new IcyHot().icyHot(t1,t2);

        //Then
        assertTrue("One temp is more than 100, another is less than 0",true);
    }
}
