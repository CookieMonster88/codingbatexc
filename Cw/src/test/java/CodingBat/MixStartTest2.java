package CodingBat;

import CodingBat.MixStart;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class MixStartTest2 {
    @Test
    public void mixStartTest2 () {
        //Given
        String str = "piz";

        //When
        boolean result = new MixStart().mixStart(str);

        //Then
        assertFalse("Wrong string",false);
    }
}
