package CodingBat;

import CodingBat.LoneTeen;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-04.
 */
public class LoneTeenTest {
    @Test
    public void loneTeenTest () {
        //Given
        int TeenA = 15;
        int TeenB = 31;

        //When
        boolean result = new LoneTeen().loneTeen(TeenA,TeenB);

        //Then
        assertTrue("There`s a Teen!",true);

    }
}
