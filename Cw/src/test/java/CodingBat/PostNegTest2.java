package CodingBat;

import CodingBat.PosNeg;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class PostNegTest2 {
    @Test
    public void postNegTest2 () {
        //Given
        int a2 = 6;
        int b2 = 8;
        boolean neg = false;

        //When
        boolean result = new PosNeg().posNeg(a2,b2,neg);

        //Then
        assertFalse("both numbers are positive!",false);

    }
}
