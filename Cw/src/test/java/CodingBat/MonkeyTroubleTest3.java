package CodingBat;

import CodingBat.MonkeyTrouble;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class MonkeyTroubleTest3 {
    @Test
    public void monkeyTroubleTest3 () {
        //Given
        boolean aSmile = false;
        boolean bSmile = true;

        //When
        boolean result = new MonkeyTrouble().monkeyTrouble(aSmile,bSmile);

        //Then
        assertFalse("The`re diff!",false);
    }
}
