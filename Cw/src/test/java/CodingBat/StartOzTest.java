package CodingBat;

import CodingBat.StartOz;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-04.
 */
public class StartOzTest {
    @Test

    public void startOzTest () {
        //Given
        String str = "ozymandias";

        //When
        String result = new StartOz().startOz(str);

        //Then
        assertEquals(result,"oz");
    }
}
