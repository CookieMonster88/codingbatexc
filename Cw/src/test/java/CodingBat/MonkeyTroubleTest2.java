package CodingBat;

import CodingBat.MonkeyTrouble;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class MonkeyTroubleTest2 {
    @Test
    public void monkeyTroubleTest2 () {
        //Given
        boolean aSmile = true;
        boolean bSmile = false;

        //When
        boolean result = new MonkeyTrouble().monkeyTrouble(aSmile,bSmile);

        //Then
        assertFalse("They1re diff!",false);
    }
}
