package CodingBat;

import CodingBat.Front3;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-04.
 */
public class Front3Test {
    @Test
    public void front3Test () {
        //Given
        String str = "Java";

        //When
        String result = new Front3().front3(str);

        //Then
        assertEquals(result,"JavJavJav");
    }
}
