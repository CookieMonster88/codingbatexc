package CodingBat;

import CodingBat.DelDel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-04.
 */
public class DelDelTest {
 @Test
    public void delDelTest () {
        //Given
        String str = "adelHello";

        //When
        String result = new DelDel().delDel(str);

        //Then
        assertEquals(result,"aHello");
    }
}
