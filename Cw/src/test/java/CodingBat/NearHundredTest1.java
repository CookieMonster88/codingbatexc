package CodingBat;

import CodingBat.NearHundred;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class NearHundredTest1 {
    @Test
    public void nearHundredTest1 () {
        // Given
        int numb = 23;

        //When
        boolean result = new NearHundred().nearHundred(numb);

        //Then
        assertFalse("Number isn`t near 100",false);
    }
}
