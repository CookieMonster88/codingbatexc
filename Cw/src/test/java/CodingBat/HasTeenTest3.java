package CodingBat;

import CodingBat.HasTeen;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-10.
 */
public class HasTeenTest3 {
    @Test
    public void hasTeenTest3 () {
        //Given
        int a3 = 4;
        int b3 = 8;
        int c3 = 16;

        //When
        boolean result = new HasTeen().hasTeen(a3,b3,c3);

        //Then
        assertTrue("There is a Teen as C",true);
    }
}
