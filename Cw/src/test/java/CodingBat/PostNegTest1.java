package CodingBat;

import CodingBat.PosNeg;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-10.
 */
public class PostNegTest1 {
    @Test
    public void postNegTest1 () {
        //Given
        int a1 = -2;
        int b1 = 7;
        boolean negative = true;

        //When
        boolean result = new PosNeg().posNeg(a1,b1,negative);

        //Then
        assertTrue("One number is neg, other isn`t",true);

    }
}
