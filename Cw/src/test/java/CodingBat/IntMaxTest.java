package CodingBat;

import CodingBat.IntMax;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-04.
 */
public class IntMaxTest {
    @Test
    public void intMaxTest () {
        //Given
        int a = 4;
        int b = 11;
        int c= 7;

        //When
        int result = new IntMax().intmax(a,b,c);

        //Then
        assertEquals(result,b);
    }
}
