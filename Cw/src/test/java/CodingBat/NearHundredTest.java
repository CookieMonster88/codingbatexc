package CodingBat;

import org.junit.Test;


import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-02.
 */
public class NearHundredTest {
    @Test
    public void nearHundredTest () {
        //Given
        int a = 95;

        //When
        assertTrue("Number is near 100",(Math.abs(100 - a) <= 10) || (Math.abs(200 - a) <=10));

        //Then
        System.out.println(a + "" + "is near 100/200");
    }
}
