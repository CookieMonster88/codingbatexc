package CodingBat;

import CodingBat.IcyHot;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class IcyHotTest1 {
    @Test
    public void icyHotTest1 () {
        //Given
        int temp1 = 111;
        int temp2 = 101;

        //When
        boolean result = new IcyHot().icyHot(temp1,temp2);

        //Then
        assertFalse("both temp are greater than 100",false);


    }
}
