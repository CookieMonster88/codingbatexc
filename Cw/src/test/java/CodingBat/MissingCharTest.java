package CodingBat;

import CodingBat.MissingChar;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-03.
 */
public class MissingCharTest {
    @Test
    public void missingCharTest() {
        //Given
        String str = "word";
        int n = 1;

        //When
        String result = new MissingChar().missingChar(str,n);

        //Then
        assertEquals(result,"rd");

        }
    }



