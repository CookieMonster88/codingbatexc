package CodingBat;

import CodingBat.HasTeen;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-04.
 */
public class HasTeenTest {
    @Test
    public void hasTeenTest () {
        //Given
        int a = 14;
        int b = 9;
        int c = 10;


        //When
        boolean result = new HasTeen().hasTeen(a,b,c);


        //Then
        assertTrue("There`s a Teen!", true);

    }
}
