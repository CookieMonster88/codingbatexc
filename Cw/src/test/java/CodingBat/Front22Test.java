package CodingBat;

import CodingBat.Front22;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-04.
 */
public class Front22Test {
    @Test
    public void front22Test () {
        //Given
        String str = "kitty";

        //When
        String result = new Front22().front22(str);

        //Then
        assertEquals(result,"kikittyki");
    }
}
