package CodingBat;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-02.
 */
public class Diff21Test {
    @Test
    public void diff21Test () {
        //Given
        int n = 19;

        //When
        assertTrue("Number is below 21",n <= 21);

        //Then
        System.out.println("Return diff =" + " " +(21-n));
    }
}
