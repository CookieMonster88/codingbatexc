package CodingBat;

import CodingBat.HasTeen;
import org.junit.Test;


import static org.junit.Assert.assertFalse;


/**
 * Created by Beata on 2017-01-10.
 */
public class HasTeenTest1 {
    @Test
    public void hasTeenTest1 () {
        //Given
        int a = 22;
        int b = 33;
        int c = 7;


        //When
        boolean result = new HasTeen().hasTeen(a,b,c);


        //Then
        assertFalse("There`s no Teen!", false);

    }
}