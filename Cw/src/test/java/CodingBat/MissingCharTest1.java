package CodingBat;

import CodingBat.MissingChar;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-10.
 */
public class MissingCharTest1 {
    @Test
    public void missingCharTest1() {
        //Given
        String str = "kitten";
        int n = 0;

        //When
        String result = new MissingChar().missingChar(str,n);

        //Then
        assertEquals(result,"itten");

    }
}