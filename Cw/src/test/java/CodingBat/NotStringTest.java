package CodingBat;

import CodingBat.NotString;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-03.
 */
public class NotStringTest {
    @Test
    public void notStringTest () {
        //Given
        String str = "cat";

        //When
        String result = new NotString().notString(str);

        //Then
        assertEquals(result,"not cat");

    }
}
