package CodingBat;

import CodingBat.PosNeg;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created by Beata on 2017-01-10.
 */
public class PostNegTest3 {
    @Test
    public void postNegTest3 () {
        //Given
        int a3 = -11;
        int b3 = -3;
        boolean neg = false;

        //When
        boolean result = new PosNeg().posNeg(a3,b3,neg);

        //Then
        assertFalse("Both numbers are neg", false);
    }
}
