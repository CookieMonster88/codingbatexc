package CodingBat;

import CodingBat.MissingChar;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-10.
 */
public class MissingCharTest2 {
    @Test
    public void missingCharTest2() {
        //Given
        String str = "kitten";
        int n = 4;

        //When
        String result = new MissingChar().missingChar(str,n);

        //Then
        assertEquals(result,"kittn");

    }
}
