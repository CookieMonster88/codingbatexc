package CodingBat;

import CodingBat.BackAround;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Beata on 2017-01-04.
 */
public class BackAroundTest {
    @Test
    public void backAroundTest () {
        //Given
        String str = "cat";


        //When
        String result = new BackAround().backAround(str);


        //Then
        assertEquals(result,"tcatt");

    }
}
