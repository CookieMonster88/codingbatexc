package CodingBat;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Beata on 2017-01-02.
 */
public class ParrotTroubleTest {
    @Test
    public void parrotTroubleTest () {
        //Given
        boolean talking = true;
        int hour = 21;

        //When
        assertTrue("The  hour is before 7.", hour <7 == true || hour>20 == true);


        //Then
        System.out.println("The hour is " + hour + "" + "We are in Trouble!");
    }
}
