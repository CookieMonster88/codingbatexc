package Warmup2;

/**
 * Created by Beata on 2017-01-12.
 */
/*
Given a non-empty string like "Code" return a string like "CCoCodCode".
 */
public class StringSplosion {
    public String stringSplosion (String str) {
        String word = "";
        for ( int i = 0; i < str.length(); i++) {
            word = word + word.substring(0,i++);
        } return word;
    }
}
