package Warmup2;

/**
 * Created by Beata on 2017-01-12.
 */
/*
Given an array of ints, return true if one of the first 4 elements
in the array is a 9. The array length may be less than 4.
 */
public class ArrayFront9 {
    public boolean arrayFront9 ( int [] numbs) {
        int endLoop = numbs.length;

        if (endLoop > 4) {
            endLoop = 4;
        }

        for (int i = 0; i < numbs.length; i++) {
            if ( numbs [i] == 9 ) {
                return true;
            }
        }
        return false;
    }

}
