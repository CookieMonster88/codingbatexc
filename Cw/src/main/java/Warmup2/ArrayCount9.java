package Warmup2;

/**
 * Created by Beata on 2017-01-12.
 */
public class ArrayCount9 {
    public int arrayCount9 (int [] numb) {
        int count = 0;
        for ( int i = 0; i < numb.length; i++) {
            if (numb [i] == 9) {
                count++;
            }
        } return count;
    }
}
