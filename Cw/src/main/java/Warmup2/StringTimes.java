package Warmup2;

/**
 * Created by Beata on 2017-01-10.
 */
//Given a string and a non-negative int n, return a larger string that is n copies of the original string.

public class StringTimes {
    public String stringTimes (String str, int n) {
        if ( n < 0 ) {
            throw new IllegalArgumentException("n cannot be negative");
        }
        String result = "";
        for ( int i = 0; i < n; i ++) {
            result = result + str;
        }
    return result;
    }
}
