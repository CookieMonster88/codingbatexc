package String2;

/**
 * Created by Beata on 2017-02-08.
 */
//Return true if the string "cat" and "dog" appear the same number of times in the given string.

public class CatDog {
    public boolean catDog (String str) {
        int countCat = 0;
        int countDog = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'c' && str.charAt(i+1) == 'a' && str.charAt(i+2) == 't') {
                countCat++;
            } else if (str.charAt(i) == 'd' && str.charAt(i+1) == 'o' && str.charAt(i+2) == 'g') {
                countDog++;
            }
            if (countCat == countDog) {
                return true;
            }
        }
        return false;
    }
}
