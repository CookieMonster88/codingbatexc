package String2;

/**
 * Created by Beata on 2017-02-08.
 */
//Given a string, return a string where for every char in the original, there are two chars.

public class DoubleChar {
    public String doubleChar (String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++ )
        result = result + str.charAt(i) + str.charAt(i);

        return result;
    }
}
