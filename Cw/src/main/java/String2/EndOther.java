package String2;

/**
 * Created by Beata on 2017-02-08.
 */
/*
Given two strings, return true if either of the strings appears at the very end of the other string,
ignoring upper/lower case differences
 */
public class EndOther {
    public boolean endOther ( String a, String b ) {
        int aLen = a.length();
        int bLen = b.length();
        String end;
        String temp;
        a = a.toLowerCase();
        b = b.toLowerCase();
        if (aLen >= bLen) {
            end = a.substring(aLen-bLen);
            temp = b;
        } else {
            end = b.substring(bLen-aLen);
            temp = a;
        }
        return (end.equals(temp));
    }
}
