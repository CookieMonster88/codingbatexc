package String2;

/**
 * Created by Beata on 2017-02-08.
 */
//Return the number of times that the string "hi" appears anywhere in the given string.
public class CountHi {
    public int countHi(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'h' && str.charAt(i + 1) == 'i') {
                count++;
            }
        }
        return count;
    }
}
