package CodingBat;

/**
 * Created by Beata on 2017-01-04.
 */
//Given 2 int values, return true if either one of them is in the range 10..20 inclusive.

public class In1020 {
    public boolean in1020 (int a, int b) {
        boolean aIsInRange = a >= 10 && a <= 20;
        boolean bIsInRange = b >= 10 && b <= 20;
        if (aIsInRange && !bIsInRange || !aIsInRange && bIsInRange) {
            return true;
        } return  false;
    }
}
