package CodingBat;

/**
 * Created by Beata on 2017-01-03.
 */
/*Given a string, take the last char and return a new
string with the last char added at the front and
back, so "cat" yields "tcatt". The original string
will be length 1 or more.
 */
public class BackAround {
    public String backAround (String str) {
        char strback = str.charAt(str.length()-1);
        if ( str.length() == 1) {
            return str + str + str;
        } else {
            return strback + str + strback;
        }
    }
}
