package CodingBat;

/**
 * Created by Beata on 2017-01-02.
 */
public class SumDouble {
    public int sumDouble(int a, int b) {
        int sum = a + b;
        if (a == b) {
            sum = sum * 2;
        }
        return sum;
    }
}
/*Given two int values, return their sum. Unless the two
     values are the same, then return double their sum.
*/