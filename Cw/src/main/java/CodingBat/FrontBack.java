package CodingBat;

/**
 * Created by Beata on 2017-01-03.
 */
//Given a string, return a new string where the first and last chars have been exchanged.

public class FrontBack {
    public String fronBack (String str) {
        if (str.length() <= 1) return str;

        String twoletters = str.substring(1, str.length()-1);

        //longer words
        String longer = str.charAt(str.length()-1) + twoletters +str.charAt(0);
        return longer;

    }
}
