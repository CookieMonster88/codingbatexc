package CodingBat;

/**
 * Created by Beata on 2017-01-02.
 */
public class Makes10 {
    public boolean makes10(int a, int b) {
        return ( a == 10 || b == 10 || a+b == 10);
    }
}
// Given 2 ints, a and b, return true if one if them is 10 or if their sum is 10.