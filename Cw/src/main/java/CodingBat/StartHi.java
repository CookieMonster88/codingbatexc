package CodingBat;

/**
 * Created by Beata on 2017-01-04.
 */
/*
Given a string, return true if the string starts with "hi" and false otherwise.
 */
public class StartHi {
    public boolean startHi (String str) {

        String hiStr = "Hi";
        int cutStr = hiStr.length();
        if (str.substring(0,cutStr).equals(hiStr)) {
            return true;
        } else {
            return false;
        }
    }
}
