package CodingBat;

/**
 * Created by Beata on 2017-01-03.
 */
/* Given a non-empty string and an int n, return a new
string where the char at index n has been removed. The value of n will be
a valid index of a char in the original string
 */

public class MissingChar {
    public String missingChar (String str, int n) {
        if (str.length() > n) {
            String word = str.substring(n + 1);
            return word;
        } else {
            return str;
        }

    }
}
