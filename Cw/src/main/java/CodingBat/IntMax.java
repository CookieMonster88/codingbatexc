package CodingBat;

/**
 * Created by Beata on 2017-01-04.
 */
//Given three int values, a b c, return the largest.

public class IntMax {
    public int intmax (int a, int b, int c) {
        int maxint;
        if ( Math.max(a,b) > c) {
            maxint =  Math.max(a,b);
        } else  {
            maxint = c;
        }
        return maxint;
    }
}
