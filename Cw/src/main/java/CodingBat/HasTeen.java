package CodingBat;

/**
 * Created by Beata on 2017-01-04.
 */
/*
We'll say that a number is "teen" if it is in the range 13..19
inclusive. Given 3 int values, return true if 1 or more of them are teen.
 */
public class HasTeen {
    public boolean hasTeen (int a, int b, int c) {
        boolean aTeen = a >= 13 && a <= 19;
        boolean bTeen = b >= 13 && b <= 19;
        boolean cTeen = c >= 13 && c <= 19;

        if ( aTeen || bTeen || cTeen) {
            return true;
        } return false;
    }
}
