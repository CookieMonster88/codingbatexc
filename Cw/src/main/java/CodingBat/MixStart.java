package CodingBat;

/**
 * Created by Beata on 2017-01-04.
 */
/*
Return true if the given string begins with "mix", except
the 'm' can be anything, so "pix", "9ix" .. all count.
 */
public class MixStart {
    public boolean mixStart (String str) {
        String keyWord = "mix";
        if (str.substring(1,3).equals(keyWord.substring(1,3))) {
            return true;
        } return false;
    }
}
