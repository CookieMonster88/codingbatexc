package Array1;

/**
 * Created by Beata on 2017-02-06.
 */
public class Unlucky1 {
    public boolean unlucky1 ( int [] nums ) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1 && nums[i+1] == 3) {
                return true;
            }
        }
        return false;
    }
}
