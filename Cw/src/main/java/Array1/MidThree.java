package Array1;

/**
 * Created by Beata on 2017-02-06.
 */
public class MidThree {
    public int[] midThree(int[] nums) {
        if (nums.length >= 3) {
            int[] threeAr = new int[3];
            int half = nums.length / 3;
            threeAr[0] = nums[half - 2];
            threeAr[1] = nums[half - 1];
            threeAr[2] = nums[half];
            return threeAr;
        } else {
            return nums;
        }
    }
}
