package Array1;

/**
 * Created by RENT on 2017-01-31.
 */
public class MakeLast {
    public int [] makeLast ( int [] nums ) {
        int doubleNumbers = nums.length * 2;
        int [] dubbArray = new int[doubleNumbers];
        dubbArray[doubleNumbers-1] = nums[nums.length-1];
        return dubbArray;
    }
}
