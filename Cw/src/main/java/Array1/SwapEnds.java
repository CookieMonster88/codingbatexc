package Array1;

/**
 * Created by Beata on 2017-02-06.
 */
public class SwapEnds {
    public int [] swapEnds ( int [] nums ) {
        int numb = nums[0];
        nums[0] = nums.length -1;
        nums[nums.length - 1] = numb;
        return nums;
    }
}
