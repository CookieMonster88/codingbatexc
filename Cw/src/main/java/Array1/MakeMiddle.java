package Array1;

/**
 * Created by Beata on 2017-02-06.
 */
public class MakeMiddle {
    public int[] makeMiddle(int[] nums) {
        int[] midArr = new int[2]; // making new 2 ints array
        int half = nums.length / 2;
        midArr[0] = nums[half - 1];
        midArr[1] = nums[half];
        return midArr;
    }
}
