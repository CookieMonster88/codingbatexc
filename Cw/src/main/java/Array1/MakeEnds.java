package Array1;

/**
 * Created by RENT on 2017-01-31.
 */
public class MakeEnds {
    public int[] makeEnds(int[] nums) {
        int[] endingArray = {nums[0], nums.length - 1};
        if (nums.length > 1) {
            return endingArray;
        } else {
            System.out.println("Array is too short");
        }
        return endingArray;
    }
}

