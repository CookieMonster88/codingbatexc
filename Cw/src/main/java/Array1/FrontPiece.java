package Array1;

/**
 * Created by Beata on 2017-02-06.
 */
public class FrontPiece {
    public int [] frontPiece ( int [] nums ) {
        int [] newAr;
        if (nums.length != 1) {
            newAr = new int[2];
            newAr[0] = nums[0];
            newAr[1] = nums[1];
        }
        if (nums.length == 1) {
            newAr = new int[1];
            newAr[0] = nums[0];
        } else {
            newAr = new int [0];
        }
        return newAr;
    }
}
