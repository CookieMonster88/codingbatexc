package Array1;

/**
 * Created by Beata on 2017-02-06.
 */
public class MaxTripple {
    public int maxTripple ( int [] nums ) {
        int max = nums[0];
        int midNumb = nums[nums.length/2];
        int lastNumb = nums[nums.length -1];

        if (max <= midNumb) {
            max = midNumb;
        }
        if (max <= lastNumb) {
            max = lastNumb;
        }
return max;
    }
}
