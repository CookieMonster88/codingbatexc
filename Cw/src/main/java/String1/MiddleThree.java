package String1;

/**
 * Created by RENT on 2017-01-23.
 */
public class MiddleThree {
    public String middleThree(String str) {
        if (str.length() > 3) {
            int chosenLetters = str.length() / 3;
            str = str.substring(chosenLetters, str.length() - chosenLetters);
        }
        return str;
    }
}
