package String1;

/**
 * Created by RENT on 2017-01-24.
 */
public class LastTwo {
    public String lastTwo ( String str ) {
        String lastLetters = str.substring(str.length()-1,str.length()) + str.substring(str.length()-2,str.length()-1);
        if (str.length() > 2) {
            str =str.substring(0,str.length()-2) + lastLetters;
        } else {
            return lastLetters;

        }
        return str;
    }
}
