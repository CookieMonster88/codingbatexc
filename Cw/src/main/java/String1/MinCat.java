package String1;

/**
 * Created by RENT on 2017-01-24.
 */
public class MinCat {
    public String minCat ( String a, String b ) {
        if ( a.length() > b.length()) {
            return a.substring(a.length()-b.length(),a.length()) + b;
        } else {
            return a + b.substring(b.length() - a.length(),b.length());
        }
    }
}
