package String1;

/**
 * Created by RENT on 2017-01-23.
 */
public class LastChars {
    public String lastChars (String a, String b) {
        if ( b.length() > 0 ) {
             return a.substring(0,1) + b.substring(b.length() - 1, b.length());
        } else {
            return a.substring(0,1) + "@";
        }
    }
}
