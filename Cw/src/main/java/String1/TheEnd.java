package String1;

/**
 * Created by RENT on 2017-01-21.
 */
public class TheEnd {
    public String theEnd (String str, boolean front) {
        if (front == true) {
            return str.substring(0,1);
        } else {
            return str.substring(str.length() - 1);
        }
    }
}
